# ttf-wps-fonts

Symbol fonts required by wps-office

https://linux.wps.cn

<br><br>
How to clone this repository:

```
git clone https://gitlab.com/azul4/content/writing-tools/wps-office/ttf-wps-fonts.git
```
